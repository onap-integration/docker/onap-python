FROM python:3.8.2-alpine3.11

LABEL maintainer="ONAP Integration team, morgan.richomme@orange.com"
LABEL Description="Reference ONAP Python 3 image"

ARG user=onap
ARG group=onap

# Create a group and user
RUN addgroup -S $group && adduser -S -D -h /usr/$user $user $group && \
    chown -R $user:$group /usr/$user &&  \
    mkdir /var/log/$user && \
    chown -R $user:$group /var/log/$user

# Tell docker that all future commands should be run as the onap user
USER $user
WORKDIR /usr/$user

ONBUILD COPY requirements.txt /requirements.txt
ONBUILD RUN pip install -r /requirements.txt
