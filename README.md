# onap-python

Thod Dockerfile aims to generate a baseline image for ONAP [1] projects
based on SECCOM recommendations [2].

It includes:

* python 3.8.2
* pip3


[1]: http://wiki.onap.org
[2]: https://wiki.onap.org/display/DW/Database%2C+Java%2C+Python%2C+Docker%2C+Kubernetes%2C+and+Image+Versions
